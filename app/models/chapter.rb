class Chapter < ApplicationRecord
  attr_accessor :checker
  
  belongs_to :previous, class_name: 'Chapter', optional: true
  belongs_to :author, class_name: 'Account', optional: true
  has_many :followers, class_name: 'Chapter', foreign_key: 'previous_id'
  has_many :subscriptions
  
  has_one_attached :audio_file
  
  after_initialize :fill_in_token
  
  validates :teaser, presence: true,
        length: { maximum: 255 }
  
  # --- SCOPES ---------------------------------
  
  scope :published, -> {
        where( status: 'public' )
        .order 'updated_at DESC' }
  scope :drafted, -> {
        where( status: 'draft' )
        .order 'updated_at DESC' }
  
  # --- INSTANCE METHODS -----------------------
  
  def to_param
    token
  end
  
  def fill_in_token
    self.token ||= Chapter.generate_token 5
  end
  
  def preview_text
    "#{author_name}: #{teaser}"
  end
  
  def author_name
    return 'niemand' unless author
    author.name_text
  end
  
  def draft?
    status == 'draft'
  end
  def published?
    status == 'public'
  end
  def hidden?
    status == 'hidden'
  end
  
  def checker_matches?
    !checker.blank? and
        checker.to_i == ( previous_chapters.size + previous_id.to_i )
  end
  
  def previous_chapters
    return [] if previous.blank?
    previous.previous_chapters << previous
  end
  
  def last_published
    return self if published?
    return nil if previous.blank?
    previous.last_published
  end
  
  def lines
    return [] if content.blank?
    content.split( "\n" ).compact
  end
  
  def teaser_words_count
    return 0 if teaser.blank?
    teaser.split( " " ).compact.size
  end
  
  def words_count
    result = teaser_words_count
    lines.each do |line|
      words = line.split( " " ).compact
      result += words.size
    end
    return result
  end
  
  def total_follower_count
    return 0 if followers.published.blank?
    followers.published.size + followers.published.inject( 0 ) { |sum,f| sum + f.total_follower_count }
  end
  
  def total_follower_words
    return 0 if followers.published.blank?
    followers.published.inject( 0 ) { |sum,f| sum + f.words_count + f.total_follower_words }
  end
  
  def longest_chapter_count
    return 1 if followers.published.blank?
    1 + followers.published.collect { |f| f.longest_chapter_count }.max
  end
  
  def longest_words
    return words_count if followers.published.blank?
    words_count + followers.published.collect { |f| f.longest_words }.max
  end
  
  def longest_branch_count
    return 0 if followers.published.blank?
    ( followers.published.size - 1 ) + followers.published.max { |a,b| a.longest_chapter_count <=> b.longest_chapter_count }.longest_branch_count
  end
  
  def total_branch_count
    return 0 if followers.published.blank?
    ( followers.published.size - 1 ) + followers.published.inject( 0 ) { |sum,f| sum + f.total_branch_count }
  end
  
  def unique_subscriptions
    result = []
    
    # add previous chapters
    previous_chapters.each do |chapter|
      chapter.subscriptions.each do |subscription|
        result << subscription unless result.detect { |s|
            s.email == subscription.email }
      end
    end
    
    # add subscriptions for this chapter
    subscriptions.each do |subscription|
      result << subscription unless result.detect { |s|
          s.email == subscription.email }
    end
    return result
  end
  
  # --- CLASS METHODS --------------------------
  
  def self.generate_token( in_length = 10, with_chars = false )
    result = ''
    # erstes Zeichen aus "sicheren" Zeichen
    random_index = rand( TOKEN_LETTERS.size )
    result += TOKEN_LETTERS[ random_index ]
    
    ( in_length - 1 ).times do
      if with_chars
        random_index = rand( TOKEN_CHARS.size )
        result += TOKEN_CHARS[ random_index ]
      else
        random_index = rand( TOKEN_LETTERS.size )
        result += TOKEN_LETTERS[ random_index ]
      end
    end
    return result
  end
  
  # --- CONSTANTS ------------------------------
  
  TOKEN_LETTERS = %w( 2 3 4 5 6 7 8 9 a b c d e f h i j k m n p q r s t u v w x y z A B C D E F H K L M N P Q R S T U V W X Y Z )
  TOKEN_CHARS = TOKEN_LETTERS.dup | %w( ! $ % & / = ? + * # < > : ) << '"' << '§' << '(' << ')' << '.'
  
end
