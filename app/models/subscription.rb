class Subscription < ApplicationRecord
  attr_accessor :checker
  
  belongs_to :chapter
  
  after_initialize :fill_in_token
  
  # --- SCOPES ---------------------------------
  
  scope :confirmed, -> {
        where( status: 'confirmed' )
        .order 'updated_at DESC' }
  
  # --- INSTANCE METHODS -----------------------
  
  def to_param
    token
  end
  
  def fill_in_token
    self.token ||= Chapter.generate_token 16
  end
  
  def checker_matches?
    !checker.blank? and
        checker.to_i == ( chapter.previous_chapters.size + 1 + chapter_id.to_i )
  end
  
  def id_hash( in_supplement = '', in_length = 10 )
    digest = Digest::SHA1.new
    digest << id.to_s
    digest << token if self.respond_to? :token
    digest << in_supplement
    digest << created_at.strftime( '%m%d%H%M%S' )
    
    result = digest.hexdigest[0..(in_length - 1)]
    logger.debug "-- id_hash( '#{in_supplement}' ):#{result}"
    return result
  end
  
end
