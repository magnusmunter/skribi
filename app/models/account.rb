# ----------------------------------------------
class Account < ApplicationRecord
  has_secure_password
  has_many :chapters, class_name: 'Chapter', foreign_key: 'author_id'
  
  after_destroy :ensure_an_admin_remains
  
  validates :email, presence: true,
        uniqueness: true
  
  class Error < StandardError
  end
  
  def preview_text
    name_text
  end
  
  def name_text
    !name.blank? ? name : 'unbekannte Person'
  end
  
private
  
  def ensure_an_admin_remains
    if Account.count.zero?
      raise Error.new "Can't delete last user"
    end
  end
  
end
