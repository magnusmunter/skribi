class SubscriptionMailer < ApplicationMailer
  default from: '✍🏼skribi Mailer <mail@skribi.io>'
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.subscription_mailer.created.subject
  #
  def created( in_subscription, in_url )
    @subscription = in_subscription
    @url = in_url
    
    mail to: @subscription.email,
          subject: 'skribi | Bitte bestätige dein Abo'
  end
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.subscription_mailer.notify.subject
  #
  def notify
    @greeting = "Hi"
    
    mail to: "to@example.org"
  end
  
end
