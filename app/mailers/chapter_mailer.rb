class ChapterMailer < ApplicationMailer
  default from: '✍🏼skribi Mailer <mail@skribi.io>'
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.chapter_mailer.created.subject
  #
  def created( in_chapter )
    # notify the author of the previous chapter
    @chapter = in_chapter.previous
    
    mail to: @chapter.author.email,
          subject: 'skribi | Dein Kapitel wird fortgesetzt'
  end
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.chapter_mailer.published.subject
  #
  def published( in_chapter, in_new_chapter, in_url )
    @chapter = in_chapter
    @new_chapter = in_new_chapter
    @url = in_url
    
    mail to: @chapter.author.email,
          subject: 'skribi | Eine Fortsetzung für Dein Kapitel ist veröffentlicht'
  end
  
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.chapter_mailer.continued.subject
  #
  def continued( in_chapter, in_new_chapter, in_url )
    @chapter = in_chapter
    @new_chapter = in_new_chapter
    @url = in_url
    
    mail to: @chapter.author.email,
          subject: 'skribi | Eine Fortsetzung Eurer Geschichte ist veröffentlicht.'
  end
  
end
