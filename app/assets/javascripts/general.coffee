# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
# ----------------------------------------------

general = () ->
  console.log '> ready @ general.coffee'
  $body = ($ 'body')
  
  show_hide = ->
    $ich = ($ this)
    ziel = $ich.attr 'href'
    hat_ziel = ( ziel isnt '#' )
    
    sel = $ich.data 'sel'
    sel = ziel unless sel
    console.log "-- aktion: ein_aus_blenden - sel:#{sel}"
    ($ sel).slideToggle 200
    return false # hat_ziel
  
  # --- Initialisierung ------------------------
  
  $body
    .off 'click.show_hide'
    .on 'click.show_hide',
        '[data-action=show_hide]',
        show_hide
  


($ document).on 'turbolinks:load', general
