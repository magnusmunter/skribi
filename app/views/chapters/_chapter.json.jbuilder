json.extract! chapter, :id, :teaser, :status, :previous_id, :author_id, :content, :created_at, :updated_at
json.url chapter_url(chapter, format: :json)
