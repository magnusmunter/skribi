json.extract! subscription, :id, :token, :status, :email, :chapter, :created_at, :updated_at
json.url subscription_url(subscription, format: :json)
