module ApplicationHelper
  
  def h_main_nav( in_status = true )
    @main_nav = in_status
  end
  def h_main_nav?
    @main_nav
  end
  def h_user_nav( in_status = true )
    @user_nav = in_status
  end
  def h_user_nav?
    @user_nav
  end
  
  def h_seitentitel
    @seitentitel || ( params[ :controller ] + ' | skribi' )
  end
  
  def h_setze_seitentitel( in_titel )
    @seitentitel = in_titel
  end
  
  def h_partial_init( in_wert, in_default )
    return in_wert unless in_wert.nil?
    return in_default
  end
  
  def h_markdown( in_text )
    in_text ||= ''
    sanitize Markdown.new( in_text ).to_html
  end
  
  def h_admin
    Account.find 1  # MMR
  end
#   def h_admin?
#     aktueller_zugang and aktueller_zugang.ist_admin?
#   end
  
  # --- OpenGraph Definitionen -----------------
  
  def h_og_type
    @og_type || 'summary'
  end
  
  def h_og_title
    @og_title || h_seitentitel
  end
  
  def h_og_description
    @og_description || 'Schreiben. Weiterschreiben. Noch weiter schreiben. Und lesen. Eine Website für Fortsetzungsgeschichten. Jemand beginnt und veröffentlicht ein kurzes Kapitel. Alle, die Lust haben, können die Geschichte dort fortsetzen. So einfach!'
  end
  
  def h_og_image
    @og_image || image_url( 'logo_wort.jpg' )
  end
  
  # --- setzen
  
  def h_type( in_type )
    @og_type = in_type
  end
  
  def h_title( in_title )
    @og_title = in_title
  end
  
  def h_description( in_description )
    @og_description = in_description
  end
  
  def h_image( in_image )
    @og_image = in_image
  end
  
end
