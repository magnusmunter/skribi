class SubscriptionsController < ApplicationController
  skip_before_action :authorize, only: [ :confirm, :new, :create, :show, :destroy ]
  before_action :set_subscription, only: %i[ confirm show edit update destroy ]
  before_action :set_chapter, only: [ :new, :create ]
  
  # --- CUSTOM METHODS -------------------------
  
  def confirm
    redirect_to @subscription.chapter, alert: 'Falscher Link, sorry!' unless params[ :t ] == @subscription.id_hash
    
    @subscription.status = 'confirmed'
    @subscription.save
  end
  
  # --- STANDARD CRUD MEHTODS ------------------
  
  # GET /subscriptions or /subscriptions.json
  def index
    @subscriptions = Subscription.all
  end
  
  # GET /subscriptions/1 or /subscriptions/1.json
  def show
  end
  
  # GET /subscriptions/new
  def new
    redirect_to :root unless @chapter
    
    @subscription = @chapter.subscriptions.build
  end
  
  # GET /subscriptions/1/edit
  def edit
  end
  
  # POST /subscriptions or /subscriptions.json
  def create
    redirect_to :root unless @chapter
    
    @subscription = @chapter.subscriptions.build( subscription_params )
    redirect_to @chapter, notice: 'Du hast die Checker-Frage vergessen, sorry!' and return unless @subscription.checker_matches?
    
    @subscription.status = 'new'
    
    respond_to do |format|
      if @subscription.save
        SubscriptionMailer.created( @subscription, confirm_subscription_url( @subscription, t: @subscription.id_hash ) ).deliver_later
        
        format.html { redirect_to @subscription, notice: "Subscription was successfully created." }
        format.json { render :show, status: :created, location: @subscription }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @subscription.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /subscriptions/1 or /subscriptions/1.json
  def update
    respond_to do |format|
      if @subscription.update( subscription_params )
        format.html { redirect_to @subscription, notice: "Subscription was successfully updated." }
        format.json { render :show, status: :ok, location: @subscription }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @subscription.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /subscriptions/1 or /subscriptions/1.json
  def destroy
    @chapter = @subscription.chapter
    @subscription.destroy
    
    respond_to do |format|
      format.html { redirect_to chapter_path( @chapter ) + "#ch#{@chapter.token}_actions", notice: "Dein Abonnement wurde gelöscht" }
      format.json { head :no_content }
    end
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_subscription
    @subscription = Subscription.where( token: params[ :id ] ).first
  end
  
  def set_chapter
    @chapter = Chapter.where( token:  params[ :chapter_id ] ).first
  end
  
  # Only allow a list of trusted parameters through.
  def subscription_params
    params.require( :subscription ).permit( :email, :chapter_id, :checker )
  end
  
end
