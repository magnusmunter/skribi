class ChaptersController < ApplicationController
  skip_before_action :authorize, only: [ :index, :show, :new, :create ]
  before_action :set_chapter, only: [ :publish, :show, :edit, :update, :destroy ]
  before_action :check_access, only: [ :publish, :edit, :update, :destroy ]
  
  # --- CUSTOM METHODS -------------------------
  
  # PATCH/PUT /chapters/1/publish
  # PATCH/PUT /chapters/1/publish.js
  def publish
    redirect_to @chapter, alert: 'Nicht dein Kapitel, sorry!' if current_account != @chapter.author and return
    
    respond_to do |format|
      if @chapter.update_attribute :status, 'public'
        # notify prevoius chapter author
        ChapterMailer.published( @chapter.previous, @chapter, url_for( @chapter ) + "#ch#{@chapter.token}" ).deliver_later
        # notify all prevoius chapter authors
        if @chapter.previous
          @chapter.previous.previous_chapters.each do |chapter|
            ChapterMailer.continued( chapter, @chapter, url_for( @chapter ) + "#ch#{@chapter.token}" ).deliver_later
          end
        end
        
        format.html { redirect_to url_for( @chapter ) + "#ch#{@chapter.token}_actions", notice: 'Kapitel erfolgreich gespeichert.' }
        format.json { render :show, status: :ok, location: @chapter }
        
        write_log 'publish_chapter', nil, @chapter
        
      else
        format.html { render :edit }
        format.json { render json: @chapter.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # --- STANDARD CRUD MEHTODS ------------------
  
  # GET /chapters
  # GET /chapters.json
  def index
    @chapters = Chapter.published.where( 'previous_id IS NULL' ).shuffle
    @max_word_count = @chapters.max_by { |c| c.longest_words }.longest_words
    logger.debug "--@max_word_count:#{@max_word_count}"
    
    write_log 'index_chapter'
  end
  
  # GET /chapters/1
  # GET /chapters/1.json
  def show
    redirect_to chapter_path( @chapter.last_published ) + "#ch#{@chapter.last_published.token}" and return unless @chapter.published? or own_chapter?
    
    @previous_chapters = @chapter.previous_chapters
    @published_followers = @chapter.followers.published
    @my_followers = @chapter.followers.select { |c| c.author == current_account }
    @followers = @published_followers | @my_followers
    
    write_log 'show_chapter', nil, @chapter
  end
  
  # GET /chapters/new
  def new
    @previous = Chapter.where( token: params[ :previous_token ] ).first
    if @previous
      @chapter = @previous.followers.build
    else
      @chapter = Chapter.new
    end
    
    @previous_chapters = @chapter.previous_chapters
  end
  
  # GET /chapters/1/edit
  def edit
    @previous_chapters = @chapter.previous_chapters
  end
  
  # POST /chapters
  # POST /chapters.json
  def create
    @chapter = Chapter.new chapter_params
    redirect_to chapters_path, notice: 'Du hast die Checker-Frage vergessen, sorry!' and return unless @chapter.checker_matches?
    
    @chapter.status = 'draft'
    @chapter.author = current_account if current_account
    
    respond_to do |format|
      if @chapter.save
        session[ :last_chapter_id ] = @chapter.id
        @previous_chapters = @chapter.previous_chapters
        
        if @chapter.author and @chapter.author.email
          ChapterMailer.created( @chapter ).deliver_later
        end
        
        format.html do
          if current_account
            redirect_to edit_chapter_path( @chapter ), notice: 'Chapter was successfully created.'
          
          else
            redirect_to new_account_path, notice: 'Chapter was successfully created.'
          end
        end
        format.json { render :show, status: :created, location: @chapter }
      else
        format.html { render :new }
        format.json { render json: @chapter.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /chapters/1
  # PATCH/PUT /chapters/1.json
  def update
    respond_to do |format|
      if @chapter.update(chapter_params)
        format.html { redirect_to url_for( @chapter ) + "#ch#{@chapter.token}_teaser", notice: 'Kapitel wurde erfolgreich geändert.' }
        format.json { render :show, status: :ok, location: @chapter }
      else
        format.html { render :edit }
        format.json { render json: @chapter.errors, status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /chapters/1
  # DELETE /chapters/1.json
  def destroy
    redirect_target = @chapter.author
    @chapter.destroy
    
    respond_to do |format|
      format.html { redirect_to redirect_target, notice: 'Kapitel wurde gelöscht.' }
      format.json { head :no_content }
    end
  end
  
private

  # Use callbacks to share common setup or constraints between actions.
  def set_chapter
    @chapter = Chapter.where( token: params[ :id ] ).first
    redirect_to( chapters_path, alert: 'Dieses Kapitel wurde nicht gefunden! Bitte überprüfe deinen Link.' ) and return unless @chapter.is_a? Chapter
  end
  
  def own_chapter?
    @chapter and @chapter.author == current_account
  end
  
  # Redirect to login if not owner
  def check_access
    redirect_to( login_path, alert: 'Not your chapter, sorry!' ) and return unless own_chapter?
  end
  
  # Never trust parameters from the scary internet, only allow the white list through.
  def chapter_params
    params.require( :chapter )
        .permit( :teaser, :status, :previous_id, :content, :audio_file, :checker )
  end
  
end
