# ----------------------------------------------
class SessionsController < ApplicationController
  skip_before_action :authorize
  
  def new
  end
  
  def create
    account = Account.find_by email: params[ :email ]
    if account.try :authenticate, params[ :password ]
      session[ :account_id ] = account.id
      
      if session[ :last_chapter_id ]
        @chapter = Chapter.find session[ :last_chapter_id ]
        @chapter.author ||= account
        @chapter.save!
        session[ :last_chapter_id ] = nil
        
        redirect_to edit_chapter_path( @chapter ), notice: "You logged in successfully."
        
      else
        redirect_to account, notice: "You logged in successfully."
      end
      
    else
      redirect_to login_url,
            alert: "Unbekannte E-Mail/Password Kombination"
    end
  end
  
  def destroy
    session[ :account_id ] = nil
    redirect_to root_url, notice: "Sie haben sich abgemeldet"
  end
  
end
