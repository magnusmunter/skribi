class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  
  before_action :authorize
  
protected
  
  def current_account
    Account.find_by id: session[ :account_id ]
  end
  helper_method :current_account
  
  def authorize
    unless current_account
      redirect_to login_path,
            notice: 'Please log in'
    end
  end
  
  # --- LOGS -----------------------------------
  
  def write_log( in_action,
        in_comment = nil,
        in_entity = nil )
    logger.debug "> write_log @ app controller"
    
    user_agent = request.env[ 'HTTP_USER_AGENT' ]
    unless user_agent =~ Log::BOT_SIGNATURES
      Log.create actor: current_account,
            action: in_action,
            comment: in_comment,
            entity: in_entity,
            user_agent: ( request.blank? ? nil : user_agent ),
            ip: ( request.blank? ? nil : request.headers[ 'X-Real-IP' ] )
    end
  end
  
end
