# ----------------------------------------------
class AccountsController < ApplicationController
  skip_before_action :authorize, only: [ :new, :create, :show ]
  before_action :set_account, only: [ :show, :edit, :update, :destroy ]
  before_action :check_access, only: [ :edit, :update, :destroy ]
  
  
  # GET /accounts
  # GET /accounts.json
  def index
    @accounts = Account.order 'created_at DESC'
  end
  
  # GET /accounts/1
  # GET /accounts/1.json
  def show
    write_log 'show_account', nil, @account
  end
  
  # GET /accounts/new
  def new
    @account = Account.new
  end
  
  # GET /accounts/1/edit
  def edit
  end
  
  # POST /accounts
  # POST /accounts.json
  def create
    @account = Account.new account_params
    @account.status = 'new'
    
    respond_to do |format|
      if @account.save
        # log it in
        session[ :account_id ] = @account.id
        
        if session[ :last_chapter_id ]
          @chapter = Chapter.find session[ :last_chapter_id ]
          @chapter.author ||= @account
          @chapter.save!
          session[ :last_chapter_id ] = nil
          
          format.html { redirect_to edit_chapter_path( @chapter ), notice: "Account #{@account.name} was successfully created." }
          
        else
          format.html { redirect_to root_url, notice: "Account #{@account.name} was successfully created." }
        end
        format.json { render :show,
              status: :created,
              location: @account }
      else
        format.html { render :new }
        format.json { render json: @account.errors,
              status: :unprocessable_entity }
      end
    end
  end
  
  # PATCH/PUT /accounts/1
  # PATCH/PUT /accounts/1.json
  def update
    redirect_to root_url, alert: 'Nicht Dein Zugang, sorry!' and return unless current_account == @account
    
    respond_to do |format|
      if @account.update account_params
        format.html { redirect_to @account,
              notice: "Account #{@account.name} was successfully updated." }
        format.json { render :show,
              status: :ok,
              location: @account }
      else
        format.html { render :edit }
        format.json { render json: @account.errors,
              status: :unprocessable_entity }
      end
    end
  end
  
  # DELETE /accounts/1
  # DELETE /accounts/1.json
  def destroy
    redirect_to root_url, alert: 'Nicht Dein Zugang, sorry!' and return unless current_account == @account
    
    @account.destroy
    respond_to do |format|
      format.html { redirect_to accounts_url, notice: 'Account was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  rescue_from 'Account::Error' do |ex|
    redirect_to accounts_url,
          notice: ex.message
  end
  
private
  # Use callbacks to share common setup or constraints between actions.
  def set_account
    @account = Account.find(params[:id])
  end
  
  # Redirect to login if not owner
  def check_access
    redirect_to( login_path, alert: 'Not your account, sorry!' ) and return unless @account == current_account
  end
  # Never trust parameters from the scary internet, only allow the white list through.
  def account_params
    params.require( :account ).permit( :email, :name, :password, :password_confirmation, :description, :url )
  end
  
end
