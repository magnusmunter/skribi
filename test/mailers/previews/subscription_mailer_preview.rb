# Preview all emails at http://localhost:3000/rails/mailers/subscription_mailer
class SubscriptionMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/subscription_mailer/created
  def created
    SubscriptionMailer.created
  end

  # Preview this email at http://localhost:3000/rails/mailers/subscription_mailer/notify
  def notify
    SubscriptionMailer.notify
  end

end
