# Preview all emails at http://localhost:3000/rails/mailers/chapter_mailer
class ChapterMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/chapter_mailer/created
  def created
    ChapterMailer.created
  end

  # Preview this email at http://localhost:3000/rails/mailers/chapter_mailer/published
  def published
    ChapterMailer.published
  end

end
