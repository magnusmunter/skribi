require 'test_helper'

class ChapterMailerTest < ActionMailer::TestCase
  test "created" do
    mail = ChapterMailer.created
    assert_equal "Created", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "published" do
    mail = ChapterMailer.published
    assert_equal "Published", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
