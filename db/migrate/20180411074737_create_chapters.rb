class CreateChapters < ActiveRecord::Migration[5.1]
  def change
    create_table :chapters do |t|
      t.string :token
      t.string :teaser
      t.string :status
      t.references :previous, foreign_key: {to_table: :chapters}
      t.references :author, foreign_key: {to_table: :accounts}
      t.text :content
      
      t.timestamps
    end
  end
end
