class CreateSubscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :subscriptions do |t|
      t.string :token
      t.string :status
      t.string :email
      t.references :chapter
      
      t.timestamps
    end
  end
end
