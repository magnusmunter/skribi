Rails.application.routes.draw do
  
  get 'history' => 'info#history'
  
  resources :subscriptions do
    member do
      get :confirm
    end
  end
  
  resources :chapters do
    
    resources :subscriptions
    
    member do
      put :publish
    end
  end
  
  controller :sessions do
    get 'login' => :new
    post 'login'=> :create
    get 'logout' => :destroy
    delete 'logout' => :destroy
  end
  
  resources :accounts
  
  get '/logs', to: 'info#logs'
  
  get '/impressum', to: 'info#impressum'
  get '/datenschutz', to: 'info#datenschutz'
  
  root to: 'info#index'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
