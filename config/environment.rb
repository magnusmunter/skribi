# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!

Rails.application.configure do
  
  # Configuring Mailer settings
  config.action_mailer.smtp_settings = {
    address:    'mail.matrix.de',
    port:       587,
    domain:     'skribi.io',
    authentication: 'plain',
    user_name:  'mail@skribi.io',
    password:   '4PU-fee-sof-eYf',
    enable_starttls_auto: true
  }
  
end
